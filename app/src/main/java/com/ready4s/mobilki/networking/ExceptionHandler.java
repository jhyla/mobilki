package com.ready4s.mobilki.networking;

import com.ready4s.mobilki.BuildConfig;
import java.util.concurrent.TimeoutException;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 12/9/2015 2:01 PM
 */
public class ExceptionHandler implements ErrorHandler {

    @Override
    public Throwable handleError(RetrofitError cause) {

        if(BuildConfig.DEBUG){
            cause.printStackTrace();
        }

        return new TimeoutException();
    }

}