package com.ready4s.mobilki.networking;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 12/9/2015 2:01 PM
 */
public class RestClient {
    public static String REST_URL = "https://restcountries.eu/";
    private static RestClient sRestClient;
    private RestApi mDefaultApiService;
    private Gson mGson;

    public static RestApi getInstance() {
        if (sRestClient == null) {
            sRestClient = new RestClient();
        }
        return sRestClient.mDefaultApiService;
    }

    public RestClient() {
        mGson = new GsonBuilder()
                .create();

        RestAdapter.Builder builder = createRestAdapterBuilder();
        mDefaultApiService = builder.build().create(RestApi.class);
    }


    private RestAdapter.Builder createRestAdapterBuilder() {
        OkHttpClient mOkHttpClient = new OkHttpClient();
        mOkHttpClient.setConnectTimeout(120, TimeUnit.SECONDS);
        mOkHttpClient.setReadTimeout(120, TimeUnit.SECONDS);

        return new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(REST_URL)
                .setClient(new OkClient(mOkHttpClient))
                .setErrorHandler(new ExceptionHandler())
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestInterceptor.RequestFacade request) {
                        request.addHeader("Accept", "application/json");
                    }
                })
                .setConverter(new GsonConverter(mGson));
    }
}
