package com.ready4s.mobilki.networking;

import com.ready4s.mobilki.model.CountryModel;

import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import retrofit.http.GET;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 12/9/2015 2:01 PM
 */
public interface RestApi {
    @GET("/rest/v1/all")
    ArrayList<CountryModel> getAllCountries() throws TimeoutException;
}
