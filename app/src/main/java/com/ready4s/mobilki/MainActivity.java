package com.ready4s.mobilki;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ready4s.mobilki.model.CountryModel;
import com.ready4s.mobilki.networking.RestClient;

import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

public class MainActivity extends AppCompatActivity {

    Button countriesButton, currencyButton;
    EditText editText;
    View progressBarTop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.editText);
        countriesButton = (Button) findViewById(R.id.countryButton);
        currencyButton = (Button) findViewById(R.id.currencyButton);
        progressBarTop = (View) findViewById(R.id.progressBarTop);

        countriesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBarTop.setVisibility(View.VISIBLE);
                GetCountriesAsyncTask asyncTask = new GetCountriesAsyncTask(MainActivity.this);
                asyncTask.execute();
            }
        });

        currencyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO
            }
        });
    }

    private class GetCountriesAsyncTask extends AsyncTask<Object, Void, ArrayList<CountryModel>> {
        private MainActivity caller;

        public GetCountriesAsyncTask(MainActivity caller) {
            this.caller = caller;
        }

        protected ArrayList<CountryModel> doInBackground(Object... params) {
            try {
                return RestClient.getInstance().getAllCountries();
            } catch (TimeoutException e) {
                return null;
            }
        }

        protected void onPostExecute(ArrayList<CountryModel> result) {
            if (result == null)
                caller.onConnectionError();
            else
                caller.onSuccess(result);
        }
    }

    private void onSuccess(ArrayList<CountryModel> result) {

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(MainActivity.this);
        builderSingle.setTitle("List of countries: ");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                MainActivity.this,
                android.R.layout.simple_list_item_1);
        for(CountryModel model : result){
            arrayAdapter.add("Country: "+model.getName()+" Capital: "+model.getCapital());
        }

        builderSingle.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        builderSingle.show();
        progressBarTop.setVisibility(View.GONE);

    }

    private void onConnectionError() {
        Toast.makeText(MainActivity.this, "Connection error", Toast.LENGTH_SHORT).show();
    }
}
