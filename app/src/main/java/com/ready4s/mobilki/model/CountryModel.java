package com.ready4s.mobilki.model;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 12/9/2015 2:08 PM
 */
public class CountryModel {
    String name;
    String capital;

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }
}
